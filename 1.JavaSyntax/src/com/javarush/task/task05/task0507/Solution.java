package com.javarush.task.task05.task0507;

/* 
Среднее арифметическое
*/

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int next = scanner.nextInt();
        double sum = 0;
        int count = 0;
        while (next != -1) {
            sum += next;
            count++;
            next = scanner.nextInt();
        }
        System.out.println(sum / count);
    }
}

