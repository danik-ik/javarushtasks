package com.javarush.task.task08.task0827;

import java.util.Date;

/* 
Работа с датой
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(isDateOdd("MAY 1 2013"));
    }

    public static boolean isDateOdd(String date) {
        Date d = new Date(date);
        Date StartOfTheYear = new Date(d.getYear(), 0, 1);
        long diff = (d.getTime() - StartOfTheYear.getTime())/(1000*24*60*60);
        return diff % 2 == 0;       
    }
}
