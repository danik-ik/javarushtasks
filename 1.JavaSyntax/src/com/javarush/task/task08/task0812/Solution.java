package com.javarush.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* 
Cамая длинная последовательность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> list = new ArrayList<Integer>();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) 
            list.add(Integer.parseInt(buffer.readLine()));
        int maxCount = 0;
        int lastKey = list.get(0) - 1; // произвольное значение, не равное первому элементу
        int currentKey;
        int currentCount = 0;
        for (int i = 0; i < list.size(); i++) {
            currentKey = list.get(i);
            if (currentKey != lastKey) {
                currentCount = 1;
                lastKey = currentKey;
            } else {
                currentCount++;
            }
            
            if (maxCount < currentCount) maxCount = currentCount;
        }
        System.out.println(maxCount);
    }
}