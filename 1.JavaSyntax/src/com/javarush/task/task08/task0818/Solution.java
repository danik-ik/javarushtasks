package com.javarush.task.task08.task0818;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Только для богачей
*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Иванов",100);
        map.put("Петров",200);
        map.put("Сидоров",300);
        map.put("Зайцын",400);
        map.put("Медведин",500);
        map.put("Снинцын",600);
        map.put("Си Ни Цын",700);
        map.put("Ли Си Цын",800);
        map.put("Иван Иваныч",900);
        map.put("Абрам Иваныч",1000);
        return map;
    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        Iterator<Map.Entry<String,Integer>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String,Integer> entry = iterator.next();
            if (entry.getValue() < 500) iterator.remove();
        }
    }

    public static void main(String[] args) {
    }
}