package com.javarush.task.task01.task0138;

/* 
Любимое стихотворение
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println("Мое любимое стихотворение:");
        System.out.println("На столе стоит арбуз,");
        System.out.println("На арбузе — муха.");
        System.out.println("Муха злится на арбуз,");
        System.out.println("Что не лезет в брюхо.");
        
    }
}