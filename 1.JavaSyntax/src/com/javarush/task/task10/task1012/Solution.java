package com.javarush.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Количество букв
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // алфавит
        String abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<Character>();
        for (int i = 0; i < abcArray.length; i++) {
            alphabet.add(abcArray[i]);
        }

        // ввод строк
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            String s = reader.readLine();
            list.add(s.toLowerCase());
        }

        String s = list.toString(); 
        // - допустимо, т.к. знаки препинания и квадратные скобки не учитываются.
        Map<Character, Integer> counts = new HashMap<Character, Integer>();
        for (Character ch: abcArray) counts.put(ch, 0);
        for (Character ch: s.toCharArray()) {
            Integer count = counts.get(ch);
            if (count != null) counts.put(ch, ++count);
        }
        for (Character ch: abcArray)
            System.out.printf("%s %s\n", ch, counts.get(ch));
    }

}
