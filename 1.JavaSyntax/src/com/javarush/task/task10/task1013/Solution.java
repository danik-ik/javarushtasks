package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

import java.util.Date;

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        private String mother;
        private String father;
        private Date bornDate;
        private String education;
        private Integer weight;
        private Integer height;

        public Human(String mother, String father, Date bornDate) {
            this.mother = mother;
            this.father = father;
            this.bornDate = bornDate;
        }

        public Human() {
        }

        public Human(int weight, int height) {

            this.weight = weight;
            this.height = height;
        }

        public Human(String mother, Date bornDate) {

            this.mother = mother;
            this.bornDate = bornDate;
        }

        public Human(Date bornDate, String education, int weight, int height) {

            this.bornDate = bornDate;
            this.education = education;
            this.weight = weight;
            this.height = height;
        }

        public Human(String mother, String father, Date bornDate, String education, int weight, int height) {

            this.mother = mother;
            this.father = father;
            this.bornDate = bornDate;
            this.education = education;
            this.weight = weight;
            this.height = height;
        }

        public Human(String mother, String father, Integer weight, Integer height) {
            this.mother = mother;
            this.father = father;
            this.weight = weight;
            this.height = height;
        }

        public Human(String mother, String father, String education) {

            this.mother = mother;
            this.father = father;
            this.education = education;
        }

        public Human(String father, String education) {

            this.father = father;
            this.education = education;
        }

        public Human(String mother, Date bornDate, Integer weight, Integer height) {

            this.mother = mother;
            this.bornDate = bornDate;
            this.weight = weight;
            this.height = height;
        }
    }
}
