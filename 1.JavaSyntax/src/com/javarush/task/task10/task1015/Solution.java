package com.javarush.task.task10.task1015;

import java.util.ArrayList;

/* 
Массив списков строк
*/

public class Solution {
    private final static String[][] data = {
            {"Красный", "Жёлтый", "Зелёный"},
            {"Красный", "Оранжевый", "Жёлтый", "Зелёный", "Голубой", "Синий", "Фиолетовый"},
            {"До", "Ре", "Ми", "Фа", "Соль", "Ля", "Си"}
    };

    public static void main(String[] args) {
        ArrayList<String>[] arrayOfStringList = createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList() {
        ArrayList<String>[] result = new ArrayList[data.length];
        for (int i = 0; i < data.length; i++) {
            result[i] = new ArrayList<String>();
            for (String s: data[i]) result[i].add(s);
        }
        return result;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList) {
        for (ArrayList<String> list : arrayOfStringList) {
            for (String s : list) {
                System.out.println(s);
            }
        }
    }
}