package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bu = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> lines = new ArrayList<>();
        int minLength = Integer.MAX_VALUE;
        int maxLength = Integer.MIN_VALUE;
        int indexOfMin = Integer.MAX_VALUE;
        int indexOfMax = Integer.MAX_VALUE;

        for (int i = 0; i < 10; i++) {
            String s = bu.readLine();
            lines.add(s);
            if (s.length() < minLength) {
                minLength = s.length();
                indexOfMin = i;
            }
            if (s.length() > maxLength) {
                maxLength = s.length();
                indexOfMax = i;
            }
        }
        System.out.println(
                indexOfMin < indexOfMax ?
                        lines.get(indexOfMin) : lines.get(indexOfMax)
        );
    }
}
