package com.javarush.task.task09.task0923;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Гласные и согласные
*/

public class Solution {
    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};
    public static char[] spaces = new char[]{' ', '\t', '\n', '\r'};
    
    public static void main(String[] args) throws Exception {
        BufferedReader bu = new BufferedReader(new InputStreamReader(System.in));
        String s = bu.readLine();
        StringBuilder vowelsOut = new StringBuilder();
        StringBuilder consonantsOut = new StringBuilder();
        for (char c: s.toCharArray()) {
            if (isVowel(c)) {
                vowelsOut.append(c);
                vowelsOut.append(' ');
            } else if (!isSpace(c)){
                consonantsOut.append(c);
                consonantsOut.append(' ');
            }
        }
        System.out.println(vowelsOut.toString());
        System.out.println(consonantsOut.toString());
    }

    // метод проверяет, гласная ли буква
    public static boolean isVowel(char c) {
        c = Character.toLowerCase(c);  // приводим символ в нижний регистр - от заглавных к строчным буквам

        for (char d : vowels)   // ищем среди массива гласных
        {
            if (c == d)
                return true;
        }
        return false;
    }

    // метод проверяет, не пробел ли это
    public static boolean isSpace(char c) {
        for (char d : spaces)   // ищем среди пробельных символов
        {
            if (c == d)
                return true;
        }
        return false;
    }
}