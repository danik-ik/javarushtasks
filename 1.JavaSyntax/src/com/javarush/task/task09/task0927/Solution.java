package com.javarush.task.task09.task0927;

import java.util.*;

/* 
Десять котов
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap() {
        String[] names = new String[] {
                "Барсик",
                "Мурзик",
                "Мявчик",
                "Котофей",
                "Кыся",
                "Брысь",
                "Рыжик",
                "Васька",
                "Хвостик",
                "Косой"
        };
        HashMap<String, Cat> cats = new HashMap<String, Cat>();
        for (String name: names) {
            cats.put(name, new Cat(name));
        }
        return cats;
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
        Set<Cat> cats = new HashSet<Cat>();
        Iterator<Cat> iterator = map.values().iterator();
        while (iterator.hasNext()) {
            cats.add(iterator.next());
        }
        return cats;
    }

    public static void printCatSet(Set<Cat> set) {
        for (Cat cat : set) {
            System.out.println(cat);
        }
    }

    public static class Cat {
        private String name;

        public Cat(String name) {
            this.name = name;
        }

        public String toString() {
            return "Cat " + this.name;
        }
    }


}
