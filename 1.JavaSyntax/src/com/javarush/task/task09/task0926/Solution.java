package com.javarush.task.task09.task0926;

import java.util.ArrayList;
import java.util.Random;

/* 
Список из массивов чисел
*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<int[]> list = createList();
        printList(list);
    }

    public static int[] lengths = new int[]{5, 2, 4, 7, 0};

    public static ArrayList<int[]> createList() {
        ArrayList<int[]> list = new ArrayList<int[]>();
        for (int l: lengths) {
            Random r = new Random();
            int [] a = new int[l]; 
            list.add(a);
            for (int i = 0; i < l; i++) {
                a[i] = r.nextInt();
            }            
        }
        return list;
    }

    public static void printList(ArrayList<int[]> list) {
        for (int[] array : list) {
            for (int x : array) {
                System.out.println(x);
            }
        }
    }
}
