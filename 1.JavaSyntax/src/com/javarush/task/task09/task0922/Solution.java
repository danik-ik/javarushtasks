package com.javarush.task.task09.task0922;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* 
Какое сегодня число?
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        String s = rd.readLine();
        SimpleDateFormat df = new SimpleDateFormat("M/d/y");
        Date d = df.parse(s);
        System.out.println(new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH).format(d).toUpperCase());
    }
}
